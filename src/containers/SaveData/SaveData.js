import React, { useState, useEffect } from 'react';
import Button from '../../components/UI/Button/Button';
import swal from 'sweetalert';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from 'axios';
import classes from './SaveData.css';
export const SaveData = (props) => {

  const [captcha, setCaptcha] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [sweetAlert, setSweetAlert] = useState(true);
  const [warningSweetAlert, setWarningSweetAlert] = useState(true);

  const [isValid, setIsValid] = useState(true);
  const { formData, formData2 } = props;

  let recaptcha = formData2.recaptchaHook;
  let selectedValuesHook = formData2.selectedValuesHook;
  let selectedAgentNoHook = formData2.selectedAgentNoHook;
  let selectedHoursNoHook = formData2.selectedHoursNoHook;
  let selectedInterventionsNoHook = formData2.selectedInterventionsNoHook;
  let carpetCleaningHook = formData2.carpetCleaningHook;
  let officeSurfaceHook = formData2.officeSurfaceHook;
  let otherServiceHook = formData2.otherServiceHook;


  const isCheckHandler = (service) => {
    return selectedValuesHook[0].filter(item => item === service).length > 0
  }
  const validityHandler = () => {
    let isValid = true;

    if(selectedValuesHook[0].length < 1){
      isValid = false;
    }

    if (isCheckHandler('Curățenie de întreținere/Birouri')) {
      isValid = false;
      if (selectedAgentNoHook[0] == 0 || selectedHoursNoHook[0] == 0 || selectedInterventionsNoHook[0] !== null) {
        isValid = true 
      }
    }
    if (isCheckHandler('Curățenie generală')) {
      isValid = officeSurfaceHook[0] !== null && isValid
      if (officeSurfaceHook[0] !== null) {
        isValid = officeSurfaceHook[0] !== "" && isValid 
      }
    }
    if (isCheckHandler('Spălare mochetă')) {
      isValid = carpetCleaningHook[0] !== null && isValid
      if (carpetCleaningHook[0] !== null) {
        isValid = carpetCleaningHook[0] !== "" && isValid 
      }
     
    }
    if (isCheckHandler('Altul')) {
      isValid = otherServiceHook[0] !== null && isValid
      if (otherServiceHook[0] !== null) {
        isValid = otherServiceHook[0] !== "" && isValid
      }
    }

    if (recaptcha[0] && selectedValuesHook[0].length > 0) {
      isValid = recaptcha[0] && isValid
    } else {
      isValid = recaptcha[0] && isValid  && false
    }

    return !isValid;
  }

  useEffect(() => {
    setIsValid(validityHandler());
    return () => {
      setIsValid(true);
    }
  }, [formData2])

  // ajax post request function with data

  const sendDataHandler = (event) => {
    setSpinner(true);
    setIsValid(true);
    event.preventDefault();

    const data = {
      contactData: formData,
      serviceData: formData2
    }

    axios.post(`${process.env.REACT_APP_SERVER_URL}/api/cere-oferta`, data)
      .then(res => {
        setSpinner(false);
        setIsValid(false);
        if (sweetAlert) {
          swal("Îți mulțumim!", "Veți primi oferta în câteva momente.", "success");
        }
        setTimeout(() => {
          setSweetAlert(false)
          window.location.href = "https://www.forbec.ro/va-multumim";
        }, 2000)

      })
      .catch(err => {
        setSpinner(false);
        setIsValid(false);
        if (warningSweetAlert) {
          swal("A intervenit o eroare!", "Încercați din nou.", "warning");
        }
        setTimeout(() => {
          setWarningSweetAlert(false)
          // window.location.reload(false);
        }, 2000)
      })
  }
 
  const style = {
    marginLeft: '50px', marginTop: '-20px'
  }
  const style2 = {
    position:'absolute'
  }
  return (
    <>
      <Button style={{ marginLeft: '20px' }} btnType='Success'
       clicked={(event) => sendDataHandler(event)} 
       disabled={isValid}>CERE OFERTA 
       <span style={style2}><div style={style} >{spinner && <Spinner />}</div></span>
       </Button>
      
    </>
  )
}
