import React, { useState, useContext,useEffect, useCallback } from 'react';
import Input from "../../components/UI/Input/Input";
import classes from './../ContactData/ServicesData/ServicesData.css';
import { OfferDataContext } from '../../Provider/OfferData';


export const OtherService = (props) => {
    const { otherServiceHook: [otherService, setOtherService] } = useContext(OfferDataContext);
    const [error,setError]=useState(null);
    const handleChange = (event) => {
        setOtherService(event.target.value)
    };
    useEffect(()=>{
        if(!otherService || otherService === null){
         const val = <p style={{fontSize:'10px',color:'darkred'}}>Acest câmp este obligatoriu</p>
         setError(val)
        }else{
         setError(null)
     }
     },[otherService])
    return <>
        <div className={classes.OfficeService} >
            <p className={classes.Paragraph}>Descriere succint serviciul dorit<span style={{ color: 'darkred' }}> *</span></p>
            <Input
                elementConfig={{ type: 'text' }}
                type={'input'}
                label={''}
                changed={handleChange}
                value={otherService}
            />
            <div className={classes.ErrorMessage} style={{marginTop:'-20px'}}>
                {error}
            </div>
            <div className={classes.Instruction}>
            <p >Introduceți un text cuprins între 1 și 1000 de cuvinte.</p>
            </div>
        </div>
    </>
}
