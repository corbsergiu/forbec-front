import React from 'react';
import {NavLink} from "react-router-dom";

const navigationItem = (props) => (
        <nav>
            <NavLink to={props.link}>{props.children}</NavLink>
        </nav>
);

export default navigationItem;