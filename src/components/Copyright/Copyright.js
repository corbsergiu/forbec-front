import React from 'react';
import classes from './Copyright.css';

const copyright =()=>{

    return (
        <div className={classes.Copyright}>
            <div className={classes.CopyrightText}>
                   <span> &copy;  2020 Forbec Services.Created by <span style={{color:'#677d8f'}}><a href="https://fivetn.com/">Fivetn Development</a></span></span>
            </div>
        </div>
    )
}

export default copyright