import React from 'react';
import classes from './Menu.css';

const Menu = () => {

    return (
        <div className={classes.Menu}>
            <h3 style={{color: '#677d8f'}}>Meniu</h3>
            <ul>
                <li><i class="fa fa-check" aria-hidden="true"></i><a
                    href="https://forbec.ro/servicii/curatenia-de-intretinere-birouri/">Curățenia de întreținere
                    birouri</a></li>
                <li><i class="fa fa-check" aria-hidden="true"></i><a href="https://forbec.ro/servicii/curatenie-generala/">Curățenie
                    generală</a></li>
                <li><i class="fa fa-check" aria-hidden="true"></i><a href="https://forbec.ro/servicii/curatenie-dupa-constructor/">Curățenie după
                    constructor</a></li>
                <li><i class="fa fa-check" aria-hidden="true"></i><a href="https://forbec.ro/servicii/curatenia-industriala/">Curățenia
                    industrială</a></li>
                <li><i class="fa fa-check" aria-hidden="true"></i><a href="https://forbec.ro/servicii/alpinismul-utilitar/">Alpinismul
                    Utilitar</a></li>
                <li><i class="fa fa-check" aria-hidden="true"></i><a href="https://forbec.ro/servicii/curatare-pardoseli/">Spălare mochetă</a></li>
                <li><a style={{color: "grey"}} href="https://www.forbec.ro/termeni-si-conditii/"> Termeni și
                    condiții</a></li>
                <li><a style={{color: "grey"}} href="https://www.forbec.ro/politica-de-confidentialitate/"> Politică de
                    confidențialitate</a></li>
            </ul>
        </div>
    )
}

export default Menu