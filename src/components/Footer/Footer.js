import React from 'react';
import classes from './Footer.css';
import AboutUs from '../Footer/AboutUs/AboutUs';
import Menu from '../Footer/Menu/Menu';
import Info from '../Footer/Info/Info';
import Copyright from "../Copyright/Copyright";

const footer = () => {

    return (
        <div className={classes.Footer}>
            <div className={classes.FooterWidth}>
                <AboutUs/>
                <Info/>
                <Menu/>
            </div>
            <Copyright/>
        </div>
    )
}

export default footer