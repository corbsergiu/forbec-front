import React from 'react';
import classes from './Info.css';
import helvaniaLogo from '../../../assets/images/helvania.png';
import helvaniaLogo2 from '../../../assets/images/helvania-copy.webp';
const Info =()=>{

    return (
        <div className={classes.Info}>
          <h3 class="ft-title">Date de contact</h3>
         <ul>
             <li><i style={{fontSize:'20px'}} class="fa fa-map-o" aria-hidden="true"></i><span>str. Cernavodă 5-9, Cluj-Napoca, România</span></li>
             <li style={{paddingTop:'10px'}} ><i style={{paddingTop:'10px',position:'absolute'}} class="fa fa-phone" aria-hidden="true"></i><span style={{paddingLeft:'35px'}}><a href="tel:+40756081784">0756 081 784</a></span> <br/><span style={{paddingLeft:'35px'}}><a href="tel:+0742238579">0742 238 579</a>
</span> </li>
             <li style={{paddingTop:'10px'}} ><i style={{paddingTop:'10px',position:'absolute'}} class="fa fa-envelope-o" aria-hidden="true"></i><span style={{paddingLeft:'35px'}}><a href="mailto:sales@forbec.ro">sales@forbec.ro</a></span> <br/><span style={{paddingLeft:'35px'}}><a href="mailto:sales@forbec.ro">office@forbec.ro</a></span> </li>
             <li style={{paddingTop:'20px'}} ><p>Owned by Helvania SA, Switzerland</p></li>
             <li><img style={{marginLeft:'-10px'}} src={helvaniaLogo2}/></li>

         </ul>
        </div>
        
    )
}

export default Info