import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import classes from './AboutUs.css';
import { SocialIcon } from 'react-social-icons';


const AboutUs =()=>{

    return (
        <div className={classes.AboutUs}>
          <h3 style={{color:'#677d8f'}}>Despre Noi</h3>
          <p>Forbec Services este o companie ce oferă servicii de curățenie generală, curățenie industrială, menținere curățenie și spălare mochete. Oferim servicii profesionale tuturor companiilor și dispunem de personal calificat, instruit tehnic, cu o experiență cumulată de peste 3 decenii.</p>

          <a href="whatsapp://call?phone=+400756081784"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
          <a href="https://www.linkedin.com/company/forbec-services/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
          <a href="https://www.facebook.com/forbec.services/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>

        </div>
    )
}

export default AboutUs