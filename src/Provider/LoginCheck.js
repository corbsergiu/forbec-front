import React, { createContext, useEffect, useState } from 'react';
export const LoginContext = createContext({});
export const LoginCheck = (props) => {
    const loginData = useState({user:'',password:''});
    const value = {
        loginData
    }
    return (
        <LoginContext.Provider value={value}>
            {props.children}
        </LoginContext.Provider>
    )
} 